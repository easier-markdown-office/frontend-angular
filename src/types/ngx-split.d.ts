declare module 'ngx-split/lib/component/split.component.d.ts' {
  export class SplitComponent {
    set gutterSize(v: number);
    get gutterSize(): number;
  }
}
