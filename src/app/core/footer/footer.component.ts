import { Component } from '@angular/core';
import { backendURL } from '../../shared/utility';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  public readonly currentYear = new Date(Date.now()).getFullYear();
  public readonly backendURL: string;

  constructor() {
    this.backendURL = `${backendURL()}api/v1`;
  }
}
