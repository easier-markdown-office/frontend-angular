import { Injectable } from '@angular/core';
import openSocket from 'socket.io-client';
import { backendURL } from '../../../shared/utility';
import { FetcherService } from '../fetcher/fetcher.service';
import { UserService } from '../user/user.service';
import { BehaviorSubject } from 'rxjs';
import { USER_EVENTS } from '../../../../submodule-models/gateway-addresses.constants';
import { EErrorCode } from '../../../../submodule-models/error-code.enum';

@Injectable({
  providedIn: 'root',
})
export class WSService {
  public socket?: SocketIOClient.Socket;
  public views = 0;

  private _isConnectedSub = new BehaviorSubject<boolean>(false);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public isConnected = this._isConnectedSub.asObservable();

  private _id = '';

  public get id(): string {
    return this._id;
  }

  public constructor(
    private readonly fetcherService: FetcherService,
    private readonly userService: UserService,
  ) {
    this.userService.isLoggedIn.subscribe((isLoggedIn) => {
      if (isLoggedIn) {
        this.connect();
      } else if (this._isConnectedSub.getValue()) {
        this.socket?.disconnect();
      }
    });
  }

  public getIsConnected(): boolean {
    return this._isConnectedSub.getValue();
  }

  private connect(): void {
    this.socket = openSocket(backendURL(), {
      query: {
        token: `Bearer ${this.fetcherService.token}`,
      },
    });

    this.socket.on('connect', () => {
      this._id = this.socket?.id ?? '';
      this._isConnectedSub.next(true);
    });

    this.socket.on('disconnect', () => {
      this._isConnectedSub.next(false);
    });

    this.socket.on('error', (data: EErrorCode) => {
      if (data === EErrorCode.authFailed) {
        this.userService.isLoggedInSub.next(false);
        this._isConnectedSub.next(false);
      }
      console.error('ERROR!!!', data === EErrorCode.authFailed);
    });

    this.socket.on(USER_EVENTS.counter, (data: number) => {
      this.views = data;
    });
  }
}
