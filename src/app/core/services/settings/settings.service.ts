import { Injectable } from '@angular/core';
import { ENotificationType } from './models/notification-type.enum';
import { ENotificationTrigger } from './models/notification-trigger.enum';
import { LOCAL_STORAGE_KEYS, parseBoolean } from '../../../shared/utility';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  public visibleRenderMarkdown = false;
  public notificationType: ENotificationType =
    ENotificationType.inApp | ENotificationType.os;

  public notificationTriggers: ENotificationTrigger =
    ENotificationTrigger.contributorJoinedLeft |
    ENotificationTrigger.invitedDischargedFromDocument;

  public showDebugOptions = false;

  public constructor() {
    const debugMode = localStorage.getItem(LOCAL_STORAGE_KEYS.debugMode);
    this.showDebugOptions = debugMode ? parseBoolean(debugMode) : false;
    console.log('debug mode', this.showDebugOptions);
  }
}
