export enum ENotificationType {
  none = 1,
  inApp = 2,
  os = 4, // aka push notifications
}
