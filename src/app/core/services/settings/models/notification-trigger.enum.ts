export enum ENotificationTrigger {
  contributorJoinedLeft = 1,
  invitedDischargedFromDocument = 2,
}
