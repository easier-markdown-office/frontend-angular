import { Injectable } from '@angular/core';
import { WSService } from '../ws/ws.service';
import {
  convertJSONClassesToRealClasses,
  getDifferent,
  mergeLines,
} from '../../../../submodule-models/utitlity.static';
import UpdateTextDTO from '../../../../submodule-models/update-text.dto';
import { AdjustClientTextDTO } from '../../../../submodule-models/adjust-client-text.dto';
import { TextChangedDTO } from '../../../../submodule-models/text-changed.dto';
import { CompleteTextDTO } from '../../../../submodule-models/complete-text.dto';
import { LINE_JOINER, LINE_SPLITTER } from '../../../shared/utility';
import LineModification from '../../../../submodule-models/line-modification.abstract';
import { LineInsertionDTO } from '../../../../submodule-models/line-insertion.dto';
import { LineDeletionDTO } from '../../../../submodule-models/line-deletion.dto';
import { LineContentModificationDTO } from '../../../../submodule-models/line-content-modification.dto';
import {
  getLineInsertionsAndDeletions,
  LineIndicesModification,
  sumUpInsertions,
} from './markdown.helper';
import { UserService } from '../user/user.service';
import {
  DOCUMENT_EVENTS,
  DOCUMENT_MESSAGES,
  ERROR_EVENTS,
} from '../../../../submodule-models/gateway-addresses.constants';
import DocumentDTO from '../../../../submodule-models/document.dto';

@Injectable({
  providedIn: 'root',
})
export class MarkdownService {
  private static PUSHING_INTERVAL = 100; // in ms
  /** Specifies the text state of the server. Necessary to determine the client changes. */
  private _serverText: string[] = [];
  /** Specifies the text state of the client. This value will use for Angular bidirectional binding (GUI) etc. */
  private _clientText = '';

  /** Indicates the last text change. Necessary for the GUI (selection range). */
  private _lastDifferent: LineModification[][] = [];

  /** Specifies the hash on which the client text is based. */
  private _textHash = 0;

  private _pushingCounter = 0;

  private _lastPushDate = 0;
  /** */
  private _lastChangedDate = 0;
  private _lastReceivedDate = 0;

  private _commiter = -1;

  // TODO: set private - just DEBUG
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public _uncommitedLines: LineIndicesModification[] = [];

  // region Properties
  public get text(): string {
    return this._clientText;
  }

  public set text(val: string) {
    this._uncommitedLines = sumUpInsertions(
      this._uncommitedLines,
      getLineInsertionsAndDeletions(this._clientText, val),
    );

    this._lastChangedDate = Date.now();

    this._clientText = val;
  }

  public get lastDifferent(): LineModification[][] {
    return this._lastDifferent;
    // return [...this._lastDifferent];
  }
  // endregion

  public constructor(
    private readonly wsService: WSService,
    private readonly userService: UserService,
  ) {
    this.wsService.isConnected.subscribe((isConnected) => {
      if (isConnected) {
        this.connected();
      } else {
        this.disconnected();
      }
    });

    this.userService.currentDocument.subscribe((doc) => {
      console.log('get text');
      if (!doc) return;

      this.wsService.socket?.emit(
        DOCUMENT_MESSAGES.completeText,
        doc.id,
        this.setTextCompletely.bind(this),
      );
    });
  }

  private connected(): void {
    if (this._commiter < 0) {
      this._commiter = setInterval(
        this.commiter.bind(this),
        MarkdownService.PUSHING_INTERVAL,
      );
    }

    this.setEventListeners();
  }

  private setEventListeners(): void {
    this.wsService.socket?.on(
      DOCUMENT_EVENTS.textChanged,
      this.textChangedFromContributor.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.created,
      this.addDocument.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.removed,
      this.removeDocument.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.renamed,
      this.renameDocument.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.collaboCreated,
      this.addCollaboratorDocument.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.collaboRenamed,
      this.renameCollaboratorDocument.bind(this),
    );

    this.wsService.socket?.on(
      DOCUMENT_EVENTS.collaboRemoved,
      this.removeCollaboratorDocument.bind(this),
    );

    this.wsService.socket?.on(ERROR_EVENTS.merging, () => {
      --this._pushingCounter;
      this.wsService.socket?.emit(
        DOCUMENT_MESSAGES.completeText,
        this.userService.getCurrentDocument()?.id,
        this.setTextCompletely.bind(this),
      );
    });
  }

  // region handle ws events
  private addDocument(doc: DocumentDTO): void {
    const currentDocs = this.userService.documentsSub.getValue();
    if (currentDocs.find((d) => d.id === doc.id)) return;

    this.userService.documentsSub.next([...currentDocs, doc]);
  }

  private addCollaboratorDocument(doc: DocumentDTO): void {
    const currentDocs = this.userService.collaboratorDocumentsSub.getValue();
    if (currentDocs.find((d) => d.id === doc.id)) return;

    this.userService.collaboratorDocumentsSub.next([...currentDocs, doc]);
  }

  private renameDocument(renamedDoc: DocumentDTO): void {
    const documents = this.userService.documentsSub.getValue();
    let found = false;
    for (const doc of documents) {
      if (doc.id !== renamedDoc.id) continue;

      found = true;
      doc.name = renamedDoc.name;
      break;
    }

    if (found) this.userService.documentsSub.next(documents);
  }

  private renameCollaboratorDocument(renamedDoc: DocumentDTO): void {
    const documents = this.userService.collaboratorDocumentsSub.getValue();
    let found = false;
    for (const doc of documents) {
      if (doc.id !== renamedDoc.id) continue;

      found = true;
      doc.name = renamedDoc.name;
      break;
    }

    if (found) this.userService.collaboratorDocumentsSub.next(documents);
  }

  private removeDocument(docID: string): void {
    this.userService.documentsSub.next([
      ...this.userService.documentsSub.getValue().filter((d) => d.id !== docID),
    ]);

    if (this.userService.getCurrentDocument()?.id === docID) {
      this.userService.changeDocument(undefined);
      this.text = '';
    }
  }

  private removeCollaboratorDocument(docID: string): void {
    this.userService.collaboratorDocumentsSub.next([
      ...this.userService.collaboratorDocumentsSub
        .getValue()
        .filter((d) => d.id !== docID),
    ]);

    if (this.userService.getCurrentDocument()?.id === docID) {
      this.userService.changeDocument(undefined);
      this.text = '';
    }
  }
  // endregion

  private disconnected(): void {
    if (this._commiter >= 0) clearInterval(this._commiter);
  }

  private setTextCompletely(data: CompleteTextDTO): void {
    console.log(data);
    this._textHash = data.hash;
    this._serverText = data.lines;
    this._clientText = data.lines.join(LINE_JOINER);
  }

  private commiter(): void {
    if (this._lastChangedDate > this._lastPushDate) {
      this.push();
    }
  }

  private push(): void {
    // let currentClientText = this._clientText;
    let clientLines = this._clientText.split(LINE_SPLITTER);
    ++this._pushingCounter;
    this._lastPushDate = Date.now();
    const sendDate = this._lastPushDate;
    const spliceLen = this._uncommitedLines.length;

    console.debug('send', this._serverText);
    this.wsService.socket?.emit(
      DOCUMENT_MESSAGES.changeText,
      {
        basedOn: this._textHash,
        changes: this.getDifferentInLineModifications(),
      } as UpdateTextDTO,

      (response: AdjustClientTextDTO) => {
        --this._pushingCounter;
        if (sendDate !== this._lastPushDate) {
          console.info('BLOCK!', response.hash);
          return;
        }

        if (sendDate < this._lastChangedDate) {
          console.info('IGNORE OR INFORMATION WILL LOST');
          return;
        }

        this._lastReceivedDate = Date.now();
        this._textHash = response.hash;

        console.debug('res', response);

        if (response.missedMods.length > 0) {
          response.missedMods = response.missedMods.map((mods) =>
            convertJSONClassesToRealClasses(mods),
          );
          response.missedMods.forEach((diffs) => {
            clientLines = mergeLines(clientLines, diffs);
          });

          this._lastDifferent = [...response.missedMods];

          console.log(sendDate, this._lastChangedDate);
          this._clientText = clientLines.join(LINE_JOINER);
        } else {
          this._lastDifferent = [];
        }
        this._serverText = this._clientText.split(LINE_SPLITTER);
        this._uncommitedLines.splice(0, spliceLen);
      },
    );
  }

  private textChangedFromContributor(data: TextChangedDTO) {
    data.changes = convertJSONClassesToRealClasses(data.changes);
    if (this._pushingCounter > 0) {
      console.info('ignore!', data.hash);
      return;
    }
    if (
      this._lastChangedDate > this._lastReceivedDate ||
      this._lastChangedDate > this._lastPushDate
    ) {
      console.info('ignore 2!', data.hash);
      return;
    }
    if (this._textHash >= data.hash) {
      console.info('ignore 3!', data.hash);
      return;
    }
    this._lastReceivedDate = Date.now();
    this._textHash = data.hash;
    this._lastDifferent = [data.changes];
    this._serverText = mergeLines(this._serverText, data.changes);
    console.debug('received', data.hash, this._serverText);
    this._clientText = this._serverText.join(LINE_JOINER);
  }

  // TODO: set private - just DEBUG
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public getDifferentInLineModifications(): LineModification[] {
    const clientLines = this._clientText.split(LINE_SPLITTER);
    const result: LineModification[] = [...this._uncommitedLines];
    const changedLines = [...this._uncommitedLines];

    const serverLines = [...this._serverText];
    for (
      let currentServerLine = 0;
      currentServerLine < serverLines.length;
      ++currentServerLine
    ) {
      const changedLine = changedLines.findIndex(
        (l) => l.index === currentServerLine,
      );

      if (
        changedLine >= 0 &&
        changedLines[changedLine] instanceof LineInsertionDTO
      ) {
        serverLines.splice(currentServerLine, 0, '');
        --currentServerLine;
      } else if (
        changedLine >= 0 &&
        changedLines[changedLine] instanceof LineDeletionDTO
      ) {
        serverLines.splice(currentServerLine, 1);
        --currentServerLine;
      } else {
        const textDiff = getDifferent(
          serverLines[currentServerLine],
          clientLines[currentServerLine],
        );
        if (
          textDiff.length > 0 &&
          !(textDiff.length === 1 && !Array.isArray(textDiff[0]))
        ) {
          result.push(
            new LineContentModificationDTO({
              index: currentServerLine,
              changes: textDiff,
            }),
          );
        }
      }

      if (changedLine >= 0) changedLines.splice(changedLine, 1);
    }

    return result;
  }
}
