import { getDifferent } from '../../../../submodule-models/utitlity.static';
import { LINE_SPLITTER } from '../../../shared/utility';
import { EChangeType } from '../../../../submodule-models/change-type.enum';
import { LineDeletionDTO } from '../../../../submodule-models/line-deletion.dto';
import { LineInsertionDTO } from '../../../../submodule-models/line-insertion.dto';

export type LineIndicesModification = LineDeletionDTO | LineInsertionDTO;

export function getLineInsertionsAndDeletions(
  oldText: string,
  newText: string,
): LineIndicesModification[] {
  const result: LineIndicesModification[] = [];

  const changes = getDifferent(oldText, newText);
  let currentLine = 0;
  let currentOffset = 0;
  for (const diff of changes) {
    if (Array.isArray(diff)) {
      const [diffType, diffContent] = diff;
      const changedLineIndices = diffContent.split(LINE_SPLITTER).length - 1;
      if (diffType === EChangeType.deletion) {
        currentOffset += diffContent.length;
        if (changedLineIndices > 0) {
          for (let i = 0; i < changedLineIndices; ++i) {
            result.push(new LineDeletionDTO({ index: currentLine }));
          }
        }
      } else {
        if (changedLineIndices > 0) {
          for (let i = 0; i < changedLineIndices; ++i) {
            result.push(new LineInsertionDTO({ index: currentLine++ }));
          }
        }
      }
    } else {
      currentLine +=
        oldText
          .substring(currentOffset, currentOffset + diff)
          .split(LINE_SPLITTER).length - 1;
      currentOffset += diff;
    }
  }

  return result;
}

export function sumUpInsertions(
  mods1: LineIndicesModification[],
  mods2: LineIndicesModification[],
): LineIndicesModification[] {
  const result = [...mods1];

  // console.log('mods', mods1, mods2);
  for (const mod2 of mods2) {
    let sameLine = -1;
    if (mod2 instanceof LineInsertionDTO) {
      sameLine = result.findIndex(
        (l) => l instanceof LineDeletionDTO && l.index === mod2.index,
      );
    } else {
      sameLine = result.findIndex(
        (l) => l instanceof LineInsertionDTO && l.index === mod2.index,
      );
    }

    if (mod2 instanceof LineDeletionDTO) {
      result.forEach((reMod) => {
        if (reMod.index >= mod2.index)
          reMod.index = Math.max(0, reMod.index - 1);
      });
    } else {
      result.forEach((reMod) => {
        if (reMod.index >= mod2.index) reMod.index += 1;
      });
    }

    if (sameLine >= 0) {
      result.splice(sameLine, 1);
    } else {
      result.push(mod2);
    }
  }

  // console.log(result);
  return result;
}
