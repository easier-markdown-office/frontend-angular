import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PushNotificationService {
  private _permission: NotificationPermission = 'default';

  public get permission(): NotificationPermission {
    return this._permission;
  }

  public get isSupported(): boolean {
    return 'Notification' in window;
  }

  public constructor() {
    this._permission = Notification.permission;
  }

  public async requestPermission(): Promise<void> {
    if (!this.isSupported) return;
    this._permission = await Notification.requestPermission();
  }

  public create(
    title: string,
    options?: NotificationOptions,
    onclick?: ((this: Notification, ev: Event) => any) | null,
  ): void {
    options = {
      ...options,
      icon: '../../../../assets/markdown.png',
      badge: '../../../../assets/markdown.png',
    };

    const notification = new Notification(title, options);
    if (onclick) notification.onclick = onclick;
  }
}
