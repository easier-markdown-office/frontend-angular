import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import DocumentDTO from '../../../../submodule-models/document.dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // should be private but I must change the values inside fetcher.service...
  public readonly isLoggedInSub = new BehaviorSubject(false);
  public readonly usernameSub = new BehaviorSubject<string | undefined>(
    undefined,
  );
  public readonly documentsSub = new BehaviorSubject<DocumentDTO[]>([]);
  public readonly collaboratorDocumentsSub = new BehaviorSubject<DocumentDTO[]>(
    [],
  );

  public readonly isLoggedIn = this.isLoggedInSub.asObservable();
  public readonly username = this.usernameSub.asObservable();
  public readonly documents = this.documentsSub.asObservable();
  public readonly collaboratorDocuments = this.collaboratorDocumentsSub.asObservable();

  private readonly _currentDocument = new BehaviorSubject<
    DocumentDTO | undefined
  >(undefined);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  public readonly currentDocument = this._currentDocument.asObservable();

  private _tabIsActive = false;

  public get isTabActive(): boolean {
    return this._tabIsActive;
  }

  public constructor() {
    this.isLoggedIn.subscribe((loggedIn) => {
      if (loggedIn) return;

      this.documentsSub.next([]);
      this.collaboratorDocumentsSub.next([]);
      this.usernameSub.next(undefined);
      this._currentDocument.next(undefined);
    });

    window.addEventListener('focus', () => (this._tabIsActive = true));
    window.addEventListener('blur', () => (this._tabIsActive = false));
  }

  public changeDocument(newDocument: DocumentDTO | undefined): void {
    this._currentDocument.next(newDocument);
  }

  public getCurrentDocument(): DocumentDTO | undefined {
    return this._currentDocument.getValue();
  }

  public getUsername(): string | undefined {
    return this.usernameSub.getValue();
  }
}
