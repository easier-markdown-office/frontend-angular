import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { PushNotificationService } from '../push-notifiction/push-notification.service';
import { SettingsService } from '../settings/settings.service';
import { ENotificationType } from '../settings/models/notification-type.enum';
import { UserService } from '../user/user.service';
import { ENotificationTrigger } from '../settings/models/notification-trigger.enum';

@Injectable({
  providedIn: 'root',
})
export class NotificationCenterService {
  public constructor(
    private readonly _snackBar: MatSnackBar,
    private readonly _pushNotification: PushNotificationService,
    private readonly _settingsService: SettingsService,
    private readonly _userService: UserService,
  ) {}

  public notify(
    notificationTrigger: ENotificationTrigger,
    message: string,
    title?: string,
    snackerOptions?: MatSnackBarConfig,
  ): void {
    if (
      (this._settingsService.notificationTriggers & notificationTrigger) !==
      notificationTrigger
    )
      return;

    if (
      !this._userService.isTabActive &&
      (this._settingsService.notificationType & ENotificationType.os) ===
        ENotificationType.os
    ) {
      this._pushNotification.create(title ?? '', { body: message });
    }

    if (
      (this._settingsService.notificationType & ENotificationType.inApp) ===
      ENotificationType.inApp
    ) {
      this._snackBar.open(message, undefined, {
        ...snackerOptions,
      });
    }
  }
}
