import { Injectable } from '@angular/core';
import { fetchRequest, LOCAL_STORAGE_KEYS } from '../../../shared/utility';
import { EHttpStatusCode } from '../../../shared/models/http-status-code.enum';
import LoginResultDTO from '../../../../submodule-models/login-result.dto';
import { UserService } from '../user/user.service';
import LoginDataDTO from '../../../../submodule-models/login-data.dto';
import UserDTO from '../../../../submodule-models/user.dto';

@Injectable({
  providedIn: 'root',
})
export class FetcherService {
  private _token: string | undefined;

  public get token(): string | undefined {
    return this._token;
  }

  public set token(token: string | undefined) {
    if (this._token === token) return;

    this._token = token;
    if (token) localStorage.setItem(LOCAL_STORAGE_KEYS.token, token);
    else localStorage.removeItem(LOCAL_STORAGE_KEYS.token);

    this.userService.isLoggedInSub.next(!!token);
  }

  public constructor(private readonly userService: UserService) {}

  public canTryLoginWithToken(): boolean {
    return !!localStorage.getItem(LOCAL_STORAGE_KEYS.token);
  }

  public async loginWithLocalToken(): Promise<boolean> {
    const localStorageToken = localStorage.getItem(LOCAL_STORAGE_KEYS.token);
    if (localStorageToken) {
      this._token = localStorageToken;

      const response = await fetchRequest('user', 'get', localStorageToken);

      if (response.status === EHttpStatusCode.OK) {
        const result = (await response.json()) as UserDTO;
        this.userService.documentsSub.next(result.documents);
        this.userService.collaboratorDocumentsSub.next(result.collaboDocs);
        this.userService.isLoggedInSub.next(true);
        this.userService.usernameSub.next(result.username);

        return true;
      }
    }

    return false;
  }

  // TODO: implement error model and return bool OR error model
  public async login(data: LoginDataDTO): Promise<string | null> {
    const response = await fetchRequest('user/login', 'post', undefined, data);

    if (response.status === EHttpStatusCode.CREATED) {
      const result = (await response.json()) as LoginResultDTO;
      this.token = result.token;
      this.userService.documentsSub.next(result.documents);
      this.userService.collaboratorDocumentsSub.next(result.collaboDocs);
      this.userService.usernameSub.next(result.username);

      return null;
    }

    return (await response.json()).message;
  }

  public async logout(): Promise<boolean> {
    const response = await fetchRequest('user/logout', 'put', this._token);

    if (response.status === EHttpStatusCode.OK) {
      this.token = undefined;
      return true;
    }

    return false;
  }
}
