import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WSService } from '../ws/ws.service';
import { CursorContributorDTO } from '../../../../submodule-models/cursor-contributor.dto';
import UpdateCursorDTO from '../../../../submodule-models/update-cursor.dto';
import {
  CURSOR_EVENTS,
  CURSOR_MESSAGES,
} from '../../../../submodule-models/gateway-addresses.constants';

@Injectable({
  providedIn: 'root',
})
export class CursorService {
  public textEditor?: HTMLTextAreaElement; // just DEBUG! // TODO: removed it!

  private _cursors = new BehaviorSubject<CursorContributorDTO[]>([]);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public cursors = this._cursors.asObservable();

  public constructor(private readonly wsService: WSService) {
    this.wsService.isConnected.subscribe((isConnected) => {
      if (isConnected) {
        this.connected();
      }
    });
  }

  public changeCursor(start: number, end: number): void {
    this.wsService.socket?.emit(CURSOR_MESSAGES.change, {
      start,
      end,
    } as UpdateCursorDTO);
  }

  public changeCursorDebug(pos: number): void {
    this.textEditor?.setSelectionRange(pos, pos);
    this.changeCursor(pos, pos);
  }

  private connected(): void {
    this.wsService.socket?.on(
      CURSOR_EVENTS.changed,
      this.handleCursorChangedFromContributor.bind(this),
    );

    this.wsService.socket?.on(
      CURSOR_EVENTS.cursors,
      (data: CursorContributorDTO[]) => {
        console.log('new cursors', data);
        this._cursors.next(data.filter((cc) => cc.id !== this.wsService.id));
      },
    );

    this.wsService.socket?.on(
      CURSOR_EVENTS.contriAdded,
      this.handleContributorJoined.bind(this),
    );

    this.wsService.socket?.on(
      CURSOR_EVENTS.contriRemoved,
      this.handleContributorLeft.bind(this),
    );
  }

  private handleCursorChangedFromContributor(cc: CursorContributorDTO): void {
    const index = this.getContributorByID(cc.id);
    if (index >= 0) {
      const cursors = this._cursors.getValue();
      [cursors[index].start, cursors[index].end] = [cc.start, cc.end];
      this._cursors.next(cursors);
    }
  }

  private handleContributorJoined(id: string): void {
    const index = this.getContributorByID(id);
    if (index < 0) {
      this._cursors.next([
        ...this._cursors.getValue(),
        { id, start: 0, end: 0 },
      ]);
    }
  }

  private handleContributorLeft(id: string): void {
    let cursors = this._cursors.getValue();
    const oldCursorsLen = cursors.length;

    cursors = cursors.filter((cc) => cc.id !== id);
    if (oldCursorsLen !== cursors.length) {
      this._cursors.next(cursors);
    }
  }

  private getContributorByID(id: string): number {
    return this._cursors.getValue().findIndex((cc) => cc.id === id);
  }
}
