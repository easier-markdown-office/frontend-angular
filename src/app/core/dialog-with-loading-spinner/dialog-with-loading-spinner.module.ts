import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogWithLoadingSpinnerComponent } from './dialog-with-loading-spinner.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DialogWithLoadingSpinnerComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatButtonModule,
  ],
  bootstrap: [DialogWithLoadingSpinnerComponent],
  exports: [DialogWithLoadingSpinnerComponent],
})
export class DialogWithLoadingSpinnerModule {}
