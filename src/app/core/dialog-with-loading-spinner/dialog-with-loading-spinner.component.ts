import { Component, Input, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-dialog-with-loading-spinner',
  templateUrl: './dialog-with-loading-spinner.component.html',
  styleUrls: ['./dialog-with-loading-spinner.component.scss'],
})
export class DialogWithLoadingSpinnerComponent {
  @Input('title')
  public title = '';
  @Input('cancelText')
  public cancelText? = 'Cancel';
  @Input('okayText')
  public okayText? = 'Okay';
  @Input('okayDisabled')
  public isOkayDisabled = false;
  @Input('okayTheme')
  public okayTheme: ThemePalette = 'primary';
  @ViewChild('submitButton', { static: true })
  public submitButton: HTMLButtonElement | undefined;

  public isLoading = false;

  constructor(
    private readonly dialogRef: MatDialogRef<DialogWithLoadingSpinnerComponent>,
  ) {}

  @Input('submitFunc')
  public submitFunc: () => Promise<boolean | any> = () =>
    new Promise<boolean>((resolve) => resolve(false));

  async submit(): Promise<void> {
    if (this.submitButton && this.submitButton.disabled) return;

    this.isLoading = true;
    const result = await this.submitFunc();
    this.isLoading = false;

    if (result) this.dialogRef.close(result);
  }
}
