import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWithLoadingSpinnerComponent } from './dialog-with-loading-spinner.component';

describe('DialogWithLoadingSpinnerComponent', () => {
  let component: DialogWithLoadingSpinnerComponent;
  let fixture: ComponentFixture<DialogWithLoadingSpinnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogWithLoadingSpinnerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWithLoadingSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
