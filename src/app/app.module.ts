import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WSService } from './core/services/ws/ws.service';
import { AppComponent } from './app.component';
import { WorkspaceModule } from './modules/workspace/workspace.module';
import { StartScreenModule } from './modules/start-screen/start-screen.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, CommonModule, WorkspaceModule, StartScreenModule],
  providers: [WSService],
  bootstrap: [AppComponent],
})
export class AppModule {}
