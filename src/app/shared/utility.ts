import { isDevMode } from '@angular/core';
import { EHttpStatusCode } from './models/http-status-code.enum';

/** This constant describes the default timeout for the fetch request. */
const DEFAULT_FETCH_TIMEOUT_MS = 1000 * 30;

export const LOCAL_STORAGE_KEYS = {
  token: 'token',
  debugMode: 'debug',
};

export function backendURL(): string {
  if (isDevMode()) return 'http://192.168.0.100:3737/';

  return 'https://markdown-lui-studio.herokuapp.com/';
}

export function isNewLine(char: string): boolean {
  return char === '\n' || char === '\r';
}

export const LINE_SPLITTER = /[\r\n]/;

export const LINE_JOINER = '\n';

export function downloadText(text: string, fileName: string): void {
  const plainText = new Blob([text]);
  downloadBlob(plainText, fileName);
}

export function downloadBlob(file: Blob, filename: string): void {
  // IE10+
  if (window.navigator.msSaveOrOpenBlob)
    window.navigator.msSaveOrOpenBlob(file, filename);
  else {
    const a = document.createElement('a'),
      url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }
}

export function parseBoolean(value: string): boolean {
  value = value.toString().toLowerCase();
  const number = parseInt(value);
  if (!isNaN(number)) return number !== 0;

  if (value === 'false') return false;

  return value === 'true';
}

/**
 * This function serves as a blanket fetch request that allows custom request through it's various parameters.
 * It's most defining feature is the fact that it sets and works with a timeout that allows the cancellation
 * of a started request after a set period of time.
 * @param url This is the url that is going to be fetched to.
 * @param method This is the (optional) method with which the fetch is going to be performed (e.g. get and so on).
 * @param token This is the (optional) authentication token required for a lot of fetch requests in the backend.
 * @param body The (optional) body of the request.
 * @param timeoutMS This is the (optional) given time after which the fetch request is going to be aborted if unable to obtain and answer prior that time mark.
 * @return Returns a response object containing the information returned from the server.
 * @source https://davidwalsh.name/fetch-timeout
 * @see Response
 */
export async function fetchRequest(
  url: string,
  method: 'get' | 'post' | 'put' | 'delete' = 'get',
  token?: string,
  // eslint-disable-next-line @typescript-eslint/ban-types
  body?: {},
  timeoutMS?: number,
): Promise<Response> {
  // set default timeout if no other one is given
  if (!timeoutMS || timeoutMS < 100) timeoutMS = DEFAULT_FETCH_TIMEOUT_MS;

  return new Promise<Response>((resolve, reject) => {
    let didTimeOut = false;
    const timeout = setTimeout(() => {
      didTimeOut = true;
      resolve(
        new Response(
          JSON.stringify({
            statusCode: EHttpStatusCode.GATEWAY_TIMEOUT,
            error: 'Timeout',
            message: 'Timeout.',
          }),
          {
            headers: {},
            status: EHttpStatusCode.GATEWAY_TIMEOUT,
            statusText: 'TIMEOUT',
          } as ResponseInit,
        ),
      );
    }, timeoutMS);

    _fetchRequest(url, method, token, body)
      .then(function (response) {
        // Clear the timeout as cleanup
        clearTimeout(timeout);
        if (!didTimeOut) resolve(response);
      })
      .catch(function (err) {
        // Rejection already happened with setTimeout
        if (didTimeOut) return;
        // Reject with error
        reject(err);
      });
  });
}

/**
 * This function serves to be a sub function that provides the actual fetch request. It takes most of the same data
 * as it's (arguable) superior function and performs the communication with the server.
 * @param url This is the url that is going to be fetched to.
 * @param method This is the (optional) method with which the fetch is going to be performed (e.g. get and so on).
 * @param token This is (optional) the authentication token required for a lot of fetch requests in the backend.
 * @param body The (optional) body of the request.
 * @private
 * @return Returns a response object containing the information returned from the server.
 */
async function _fetchRequest(
  url: string,
  method: 'get' | 'post' | 'put' | 'delete' = 'get',
  token?: string,
  // eslint-disable-next-line @typescript-eslint/ban-types
  body?: {},
): Promise<Response> {
  const fetchObj: RequestInit = {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // for optional authentication set the token if given, otherwise omit it.
  if (token)
    fetchObj.headers = {
      ...fetchObj.headers,
      Authorization: `Bearer ${token}`,
    };

  // the body is also optional.
  if (body) fetchObj.body = JSON.stringify(body);

  return await fetch(`${backendURL()}${url}`, fetchObj);
}
