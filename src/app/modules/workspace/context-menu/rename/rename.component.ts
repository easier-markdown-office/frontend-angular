import { Component, Inject, ViewChild } from '@angular/core';
import DocumentDTO from '../../../../../submodule-models/document.dto';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DOCUMENT_MESSAGES } from '../../../../../submodule-models/gateway-addresses.constants';
import { WSService } from '../../../../core/services/ws/ws.service';
import { isDocumentNameValid } from '../../../../../submodule-models/utitlity.static';
import { DialogWithLoadingSpinnerComponent } from '../../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.component';

@Component({
  selector: 'app-rename',
  templateUrl: './rename.component.html',
  styleUrls: ['./rename.component.scss'],
})
export class RenameComponent {
  @ViewChild('dialog', { static: true })
  public dialogComponent: DialogWithLoadingSpinnerComponent | undefined;

  public newName: string;

  constructor(
    private readonly wsService: WSService,
    @Inject(MAT_DIALOG_DATA) public data: DocumentDTO,
  ) {
    this.newName = data.name;
    this.nameIsValidate = isDocumentNameValid;
  }

  public nameIsValidate: (name: string) => boolean = () => true;

  async emitButtonSubmit(): Promise<void> {
    if (
      !this.dialogComponent ||
      !this.dialogComponent.submitButton ||
      this.dialogComponent.submitButton.disabled
    )
      return;

    await this.dialogComponent.submit();
  }

  async submit(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.wsService.socket?.emit(
        DOCUMENT_MESSAGES.rename,
        new DocumentDTO({ id: this.data.id, name: this.newName }),
        (data: any) => {
          console.log(data);
          resolve(true);
        },
      );
    });
  }
}
