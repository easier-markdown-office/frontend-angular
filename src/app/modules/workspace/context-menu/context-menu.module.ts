import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContextMenuComponent } from './context-menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { DialogWithLoadingSpinnerModule } from '../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.module';

@NgModule({
  declarations: [ContextMenuComponent],
  imports: [
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    DialogWithLoadingSpinnerModule,
  ],
  exports: [ContextMenuComponent],
})
export class ContextMenuModule {}
