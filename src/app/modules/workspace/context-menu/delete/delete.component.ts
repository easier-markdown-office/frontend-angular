import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import DocumentDTO from '../../../../../submodule-models/document.dto';
import { DOCUMENT_MESSAGES } from '../../../../../submodule-models/gateway-addresses.constants';
import { WSService } from '../../../../core/services/ws/ws.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
})
export class DeleteComponent {
  constructor(
    private readonly wsService: WSService,
    @Inject(MAT_DIALOG_DATA) public data: DocumentDTO,
  ) {}

  async submit(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.wsService.socket?.emit(
        DOCUMENT_MESSAGES.delete,
        this.data.id,
        (data: boolean) => {
          if (data) resolve(true);
          else reject(false);
        },
      );
    });
  }
}
