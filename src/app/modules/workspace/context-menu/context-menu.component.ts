import { Component, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import DocumentDTO from '../../../../submodule-models/document.dto';
import { RenameComponent } from './rename/rename.component';
import { DeleteComponent } from './delete/delete.component';
import { downloadText, LINE_JOINER } from '../../../shared/utility';
import { DOCUMENT_MESSAGES } from '../../../../submodule-models/gateway-addresses.constants';
import { CompleteTextDTO } from '../../../../submodule-models/complete-text.dto';
import { WSService } from '../../../core/services/ws/ws.service';
import { MatDialog } from '@angular/material/dialog';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { UserService } from '../../../core/services/user/user.service';
import { ContributorsDialogComponent } from '../contributors-dialog/contributors-dialog.component';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
})
export class ContextMenuComponent {
  @ViewChild(MatMenuTrigger, { static: true })
  public contextMenu?: MatMenuTrigger;

  public contextMenuPosition = { x: '0', y: '0' };

  constructor(
    private readonly wsService: WSService,
    private readonly dialog: MatDialog,
    private readonly markdownService: MarkdownService,
    private readonly userService: UserService,
  ) {}

  openContextMenu(
    position: { clientX: number; clientY: number },
    item: DocumentDTO,
  ): void {
    if (!this.contextMenu) return;

    this.contextMenuPosition.x = position.clientX + 'px';
    this.contextMenuPosition.y = position.clientY + 'px';

    this.contextMenu.menuData = { item: item };
    this.contextMenu.openMenu();
  }

  openRenameDialog(doc: DocumentDTO): void {
    const dialogRef = this.dialog.open(RenameComponent, {
      width: '550px',
      data: doc,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  openDeleteDialog(doc: DocumentDTO): void {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '550px',
      data: doc,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  openContributorsDialog(doc: DocumentDTO): void {
    const dialogRef = this.dialog.open(ContributorsDialogComponent, {
      width: '550px',
      data: doc,
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  downloadDocument(toDownloadDoc: DocumentDTO): void {
    const currentDoc = this.userService.getCurrentDocument();
    if (currentDoc && currentDoc.id === toDownloadDoc.id) {
      downloadText(this.markdownService.text, `${currentDoc.name}.md`);
      return;
    }

    this.wsService.socket?.emit(
      DOCUMENT_MESSAGES.completeTextWithoutSideEffects,
      toDownloadDoc.id,
      (doc: CompleteTextDTO) => {
        downloadText(doc.lines.join(LINE_JOINER), `${toDownloadDoc.name}.md`);
      },
    );
  }
}
