import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { WSService } from '../../../core/services/ws/ws.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import DocumentDTO from '../../../../submodule-models/document.dto';
import { AddComponent } from './add/add.component';
import { FetcherService } from '../../../core/services/fetcher/fetcher.service';
import { fetchRequest } from '../../../shared/utility';
import { EHttpStatusCode } from '../../../shared/models/http-status-code.enum';
import { MatSnackBar } from '@angular/material/snack-bar';
import ContributorInfoDTO from '../../../../submodule-models/contributor-info.dto';
import { ContriListComponent } from './contri-list/contri-list.component';

@Component({
  selector: 'app-contributors-dialog',
  templateUrl: './contributors-dialog.component.html',
  styleUrls: ['./contributors-dialog.component.scss'],
})
export class ContributorsDialogComponent implements OnInit {
  @ViewChild('contributorsList', { static: true })
  public contributorsList: ContriListComponent | undefined;

  public isLoading = false;
  public contributors: ContributorInfoDTO[] = [];

  constructor(
    private readonly dialog: MatDialog,
    private readonly wsService: WSService,
    private readonly fetcher: FetcherService,
    private readonly _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: DocumentDTO,
  ) {}

  async submit(): Promise<void> {
    //
  }

  async ngOnInit(): Promise<void> {
    this.isLoading = true;

    const res = await fetchRequest(
      `document/${this.data.id}/contributors`,
      'get',
      this.fetcher.token,
    );
    const resBody = await res.json();
    if (res.status === EHttpStatusCode.OK) {
      this.contributors = resBody;
    } else {
      this._snackBar.open(resBody.message, 'Okay!', {
        duration: 8000,
      });
    }

    this.isLoading = false;
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddComponent, {
      width: '550px',
      data: this.data,
    });

    dialogRef.afterClosed().subscribe((result: ContributorInfoDTO) => {
      this.contributors = [...this.contributors, result]; // push doesn't work... it must be a reset
    });
  }
}
