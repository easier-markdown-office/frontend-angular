import { Component, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import ContributorInfoDTO from '../../../../../../submodule-models/contributor-info.dto';

@Component({
  selector: 'app-remove',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.scss'],
})
export class RemoveComponent {
  constructor(
    private readonly _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA)
    public data: { contri: ContributorInfoDTO; submit: () => Promise<boolean> },
  ) {
    //
  }

  async submit(): Promise<boolean> {
    return this.data.submit();
  }
}
