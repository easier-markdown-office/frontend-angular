import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContriListComponent } from './contri-list.component';

describe('ContriListComponent', () => {
  let component: ContriListComponent;
  let fixture: ComponentFixture<ContriListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContriListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContriListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
