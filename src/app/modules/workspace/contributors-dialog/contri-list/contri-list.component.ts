import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import DocumentDTO from '../../../../../submodule-models/document.dto';
import ContributorInfoDTO from '../../../../../submodule-models/contributor-info.dto';
import { fetchRequest } from '../../../../shared/utility';
import { FetcherService } from '../../../../core/services/fetcher/fetcher.service';
import { EHttpStatusCode } from '../../../../shared/models/http-status-code.enum';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RemoveComponent } from './remove/remove.component';

@Component({
  selector: 'app-contri-list',
  templateUrl: './contri-list.component.html',
  styleUrls: ['./contri-list.component.scss'],
})
export class ContriListComponent {
  @Input('contributors')
  public contributors: ContributorInfoDTO[] = [];
  @Output()
  public contributorsChange: EventEmitter<
    ContributorInfoDTO[]
  > = new EventEmitter<ContributorInfoDTO[]>();

  public displayedColumns: string[] = [
    'username',
    'email',
    'actions',
    'select',
  ];
  public selection = new SelectionModel<ContributorInfoDTO>(true, []);
  public disabledRows: ContributorInfoDTO[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: DocumentDTO,
    private readonly fetchService: FetcherService,
    private readonly _snackBar: MatSnackBar,
    private readonly dialog: MatDialog,
  ) {}

  isRowDisabled(row: ContributorInfoDTO): boolean {
    return !!this.disabledRows.find((r) => r === row);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.contributors.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(): void {
    this.isAllSelected()
      ? this.selection.clear()
      : this.selection.select(...this.contributors);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ContributorInfoDTO): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
  }

  openRemoveDialog(contri: ContributorInfoDTO): void {
    this.dialog.open(RemoveComponent, {
      width: '550px',
      data: { contri: contri, submit: () => this.removeContributor(contri) },
    });
  }

  private async removeContributor(
    contri: ContributorInfoDTO,
  ): Promise<boolean> {
    this.disabledRows.push(contri);
    const res = await fetchRequest(
      `document/${this.data.id}/discharge`,
      'put',
      this.fetchService.token,
      { email: contri.email },
    );
    this.disabledRows = this.disabledRows.filter(
      (r) => r.email !== contri.email,
    );

    if (res.status === EHttpStatusCode.OK) {
      this.contributors = this.contributors.filter(
        (r) => r.email !== contri.email,
      );
      this.selection.deselect(contri);
      this.contributorsChange.emit(this.contributors);
      return true;
    } else {
      const body = await res.json();
      this._snackBar.open(body.message, 'Okay!', {
        duration: 8000,
      });
    }

    return false;
  }
}
