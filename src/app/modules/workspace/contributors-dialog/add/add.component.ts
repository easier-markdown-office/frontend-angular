import { Component, Inject, ViewChild } from '@angular/core';
import { WSService } from '../../../../core/services/ws/ws.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import DocumentDTO from '../../../../../submodule-models/document.dto';
import { DialogWithLoadingSpinnerComponent } from '../../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.component';
import { fetchRequest } from '../../../../shared/utility';
import { EHttpStatusCode } from '../../../../shared/models/http-status-code.enum';
import { FetcherService } from '../../../../core/services/fetcher/fetcher.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent {
  @ViewChild('dialog', { static: true })
  public dialogComponent: DialogWithLoadingSpinnerComponent | undefined;

  public emailAddress = '';

  constructor(
    private readonly wsService: WSService,
    private readonly fetchService: FetcherService,
    private readonly _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: DocumentDTO,
  ) {}

  checkEmailValidate(): boolean {
    return (
      !!this.emailAddress &&
      this.emailAddress.length > 3 &&
      this.emailAddress.includes('@') &&
      !this.emailAddress.endsWith('@')
    );
  }

  async emitButtonSubmit(): Promise<void> {
    if (
      !this.dialogComponent ||
      !this.dialogComponent.submitButton ||
      this.dialogComponent.submitButton.disabled
    )
      return;

    await this.dialogComponent.submit();
  }

  async submit(): Promise<boolean> {
    const res = await fetchRequest(
      `document/${this.data.id}/invite`,
      'put',
      this.fetchService.token,
      { email: this.emailAddress },
    );
    const body = await res.json();
    if (res.status === EHttpStatusCode.OK) {
      return body;
    } else {
      this._snackBar.open(body.message, 'Okay!', {
        duration: 8000,
      });
    }

    return false;
  }
}
