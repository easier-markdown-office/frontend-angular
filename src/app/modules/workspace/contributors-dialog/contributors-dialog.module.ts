import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContributorsDialogComponent } from './contributors-dialog.component';
// import { MatTableModule } from '@angular/material/table';
import { DialogWithLoadingSpinnerModule } from '../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.module';
import { ContriListComponent } from './contri-list/contri-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AddComponent } from './add/add.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { RemoveComponent } from './contri-list/remove/remove.component';

@NgModule({
  declarations: [
    ContributorsDialogComponent,
    ContriListComponent,
    AddComponent,
    RemoveComponent,
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    DialogWithLoadingSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatInputModule,
    FormsModule,
  ],
  bootstrap: [ContributorsDialogComponent],
  exports: [ContributorsDialogComponent],
})
export class ContributorsDialogModule {}
