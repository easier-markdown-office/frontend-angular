import { Component } from '@angular/core';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';

@Component({
  selector: 'app-markdown-preview',
  templateUrl: './markdown-preview.component.html',
  styleUrls: ['./markdown-preview.component.scss'],
})
export class MarkdownPreviewComponent {
  constructor(public readonly markdownService: MarkdownService) {}
}
