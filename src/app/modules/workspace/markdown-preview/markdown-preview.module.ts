import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { MarkdownPreviewComponent } from './markdown-preview.component';
import { ShowdownModule } from 'ngx-showdown';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import Showdown from 'showdown';

// inspired by: https://github.com/showdownjs/showdown/issues/337
const linkExtension: Showdown.FilterExtension = {
  type: 'output',
  filter: (text: string) =>
    text.replace(/(?<=<a href=.*?)>/, 'target="_blank">'),
};

@NgModule({
  declarations: [MarkdownPreviewComponent],
  imports: [
    BrowserModule,
    CommonModule,
    ShowdownModule.forRoot({
      emoji: true,
      flavor: 'github',
      extensions: [linkExtension],
    }),
  ],
  providers: [MarkdownService],
  exports: [MarkdownPreviewComponent],
  bootstrap: [MarkdownPreviewComponent],
})
export class MarkdownPreviewModule {}
