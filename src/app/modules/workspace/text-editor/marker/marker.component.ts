import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { CursorService } from '../../../../core/services/cursor/cursor.service';
import { MarkdownService } from '../../../../core/services/markdown/markdown.service';
import { CursorContributorDTO } from '../../../../../submodule-models/cursor-contributor.dto';
import { Rectangle } from './models/rectangle.model';

@Component({
  selector: 'app-marker',
  templateUrl: './marker.component.html',
  styleUrls: ['./marker.component.scss'],
  animations: [
    trigger('fadeAnimation', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [style({ opacity: 0 }), animate(200)]),
      transition(':leave', animate(400, style({ opacity: 0 }))),
    ]),
  ],
})
export class MarkerComponent {
  @ViewChild('fakeText')
  public fakeText: ElementRef<HTMLDivElement> | undefined;
  @Input('markdownText')
  public text: HTMLTextAreaElement | undefined;

  public contributorsSelections: Rectangle[] = [];
  public contributorsCursors: Rectangle[] = [];

  private _cursors: CursorContributorDTO[] = [];

  constructor(
    public readonly markdownService: MarkdownService,
    private readonly cursorService: CursorService,
  ) {
    this.cursorService.cursors.subscribe((cursors) => {
      this._cursors = [...cursors];

      this.updateCursor();
    });
  }

  onResized(): void {
    this.updateCursor();
  }

  private updateCursor(): void {
    if (!this.fakeText) return;

    const newSelections: Rectangle[] = [];
    const newCursors: Rectangle[] = [];
    const rect = this.fakeText.nativeElement.getBoundingClientRect();
    for (const cc of this._cursors) {
      try {
        const range = document.createRange();
        range.setStart(
          this.fakeText.nativeElement.firstChild as ChildNode,
          cc.start,
        );
        range.setEnd(
          this.fakeText.nativeElement.firstChild as ChildNode,
          cc.end,
        );

        const clientRect = range.getBoundingClientRect();
        const pos = {
          x: clientRect.x - rect.x,
          y: clientRect.y - rect.y,
          width: clientRect.width,
          height: clientRect.height,
        };
        if (cc.start === cc.end) newCursors.push(pos);
        else newSelections.push(pos);
      } catch (e) {
        console.debug('Range error (?)', e);
      }
    }

    this.contributorsSelections = newSelections;
    this.contributorsCursors = newCursors;
  }
}
