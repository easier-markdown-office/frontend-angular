import {
  forwardRef,
  Renderer2,
  ElementRef,
  Directive,
  Input,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { EChangeType } from '../../../../submodule-models/change-type.enum';
import { CursorService } from '../../../core/services/cursor/cursor.service';
import { LineInsertionDTO } from '../../../../submodule-models/line-insertion.dto';
import { LineContentModificationDTO } from '../../../../submodule-models/line-content-modification.dto';
import { LineDeletionDTO } from '../../../../submodule-models/line-deletion.dto';
import { LINE_SPLITTER } from '../../../shared/utility';

export const SELECTION_KEEPER_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectionKeeperDirective),
  multi: true,
};

/**
 * @remark This class is inspired by https://stackoverflow.com/a/44465729
 */
@Directive({
  selector: 'textarea[ngModel]',
  host: {
    '(input)': 'handleInput($event.target.value)',
    '(blur)': 'onTouched()',
  },
  providers: [SELECTION_KEEPER_VALUE_ACCESSOR],
})
export class SelectionKeeperDirective implements ControlValueAccessor {
  @Input('afterChanged')
  public afterChanged: (() => void) | undefined;

  @Input('afterExternalChanged')
  public afterExternalChanged: (() => void) | undefined;

  private _lineOffsets: number[] = [];

  constructor(
    private readonly _renderer: Renderer2,
    private readonly _elementRef: ElementRef<HTMLTextAreaElement>,
    private readonly markdownService: MarkdownService,
    private readonly cursorService: CursorService,
  ) {}

  private static computeLineOffsets(text: string): number[] {
    const lines = text.split(LINE_SPLITTER);
    let lineOffsets = lines
      .splice(0, lines.length - 1) // the last one is not a new line, so cut off!
      .map((l) => l.length + 1); // each new line char must included into the offset, so +1

    // in moment lineOffsets includes just the line length (included the new line char)
    // now convert all lengths to an offset
    for (let i = lineOffsets.length - 1; i >= 0; --i) {
      for (let j = i - 1; j >= 0; --j) {
        lineOffsets[i] += lineOffsets[j];
      }
    }

    lineOffsets = lineOffsets.map((l) => l - 1); // currently, all offset based on 1...n but we want the offset with 0...n-1
    return lineOffsets;
  }

  public onChange: (val: any) => void = (): void => {
    /* */
  };
  public onTouched = (): void => {
    /* */
  };

  writeValue(value: unknown): void {
    const oldSelectionStart = this._elementRef.nativeElement.selectionStart;
    const oldSelectionEnd = this._elementRef.nativeElement.selectionEnd;
    this._lineOffsets = SelectionKeeperDirective.computeLineOffsets(
      this._elementRef.nativeElement.value,
    );
    this._renderer.setProperty(this._elementRef.nativeElement, 'value', value);

    const newCursorPos = this.computeNewCursorPosition(
      oldSelectionStart,
      oldSelectionEnd,
    );

    if (this.markdownService._uncommitedLines.length > 0) {
      console.error('_uncommitedLines are greater than 0!');
    }

    this._elementRef.nativeElement.setSelectionRange(
      newCursorPos.start,
      newCursorPos.end,
    );

    this.cursorService.changeCursor(newCursorPos.start, newCursorPos.end);

    console.log(
      oldSelectionStart,
      oldSelectionEnd,
      ' x ',
      newCursorPos.start,
      newCursorPos.end,
      this.markdownService.lastDifferent[0],
      this._elementRef.nativeElement.value,
    );
    if (this.afterExternalChanged) this.afterExternalChanged();
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._renderer.setProperty(
      this._elementRef.nativeElement,
      'disabled',
      isDisabled,
    );
  }

  handleInput(value: unknown): void {
    this.onChange(value);
    if (this.afterChanged) this.afterChanged();
  }

  private computeNewCursorPosition(
    oldSelectionStart: number,
    oldSelectionEnd: number,
  ): { start: number; end: number } {
    let selectionStartLine = this.getLineIndexFromOffset(oldSelectionStart);
    let selectionEndLine = this.getLineIndexFromOffset(oldSelectionEnd);

    for (const commits of this.markdownService.lastDifferent) {
      for (const lineModifications of commits) {
        if (lineModifications instanceof LineInsertionDTO) {
          this.insertLineToOffset(lineModifications.index);

          if (lineModifications.index <= selectionStartLine) {
            ++oldSelectionStart;
            ++oldSelectionEnd;
            ++selectionStartLine;
            ++selectionEndLine;
          } else if (lineModifications.index <= selectionEndLine) {
            ++oldSelectionEnd;
            ++selectionEndLine;
          }
        } else if (lineModifications instanceof LineDeletionDTO) {
          this.deleteLineToOffset(lineModifications.index);

          if (lineModifications.index <= selectionStartLine) {
            --oldSelectionStart;
            --oldSelectionEnd;
            --selectionStartLine;
            --selectionEndLine;
          } else if (lineModifications.index <= selectionEndLine) {
            --oldSelectionEnd;
            --selectionEndLine;
          }
        } else if (lineModifications instanceof LineContentModificationDTO) {
          let currentDiffOffset =
            (this._lineOffsets[lineModifications.index - 1] ?? -1) + 1;
          for (const diff of lineModifications.changes) {
            if (currentDiffOffset > oldSelectionEnd) break;

            if (Array.isArray(diff)) {
              const [diffType, diffItem] = diff;
              if (diffType === EChangeType.insertion) {
                if (currentDiffOffset < oldSelectionStart) {
                  oldSelectionStart += diffItem.length;
                  oldSelectionEnd += diffItem.length;
                } else if (currentDiffOffset <= oldSelectionEnd) {
                  oldSelectionEnd += diffItem.length;
                }

                currentDiffOffset += diffItem.length;
              } else if (diffType === EChangeType.deletion) {
                if (currentDiffOffset === oldSelectionStart) {
                  oldSelectionEnd -= diffItem.length;
                } else if (
                  currentDiffOffset + diffItem.length <
                  oldSelectionStart
                ) {
                  oldSelectionStart -= diffItem.length;
                  oldSelectionEnd -= diffItem.length;
                }
                // cross
                else if (
                  currentDiffOffset < oldSelectionStart &&
                  currentDiffOffset + diffItem.length > oldSelectionStart
                ) {
                  const diff =
                    currentDiffOffset + diffItem.length - oldSelectionStart;
                  const removedPart = diffItem.length - diff;
                  oldSelectionStart -= removedPart;
                  oldSelectionEnd -= diffItem.length;
                }
                // cross 2
                else if (
                  currentDiffOffset < oldSelectionEnd &&
                  currentDiffOffset + diffItem.length > oldSelectionEnd
                ) {
                  const diff =
                    currentDiffOffset + diffItem.length - oldSelectionEnd;
                  const removedPart = diffItem.length - diff;
                  oldSelectionEnd -= removedPart;
                } else if (
                  currentDiffOffset <=
                  oldSelectionEnd + diffItem.length
                ) {
                  oldSelectionEnd -= diffItem.length;
                }
              }
            } else {
              currentDiffOffset += diff;
            }
          }
        }
      }
    }

    return { start: oldSelectionStart, end: oldSelectionEnd };
  }

  private getLineIndexFromOffset(offset: number): number {
    let currentLine = 0;
    for (const lineOffset of this._lineOffsets) {
      if (lineOffset >= offset) break;

      ++currentLine;
    }

    return currentLine;
  }

  private insertLineToOffset(insertLineIndex: number): void {
    let offsetOfNewLine = 0;
    for (let i = 0; i < this._lineOffsets.length; ++i) {
      if (i < insertLineIndex) {
        offsetOfNewLine += this._lineOffsets[i];
      } else if (i === insertLineIndex) {
        this._lineOffsets.splice(i, 0, offsetOfNewLine);
        ++i;
      } else {
        this._lineOffsets[i] += 1;
      }
    }
  }

  private deleteLineToOffset(deleteLineIndex: number): void {
    let offsetOfDeleteLine = -1;
    for (let i = 0; i < this._lineOffsets.length; ++i) {
      if (i === deleteLineIndex && offsetOfDeleteLine < 0) {
        offsetOfDeleteLine = this._lineOffsets[i];
        this._lineOffsets.splice(i, 1);
        --i;
      } else if (i >= deleteLineIndex && offsetOfDeleteLine >= 0) {
        this._lineOffsets[i] -= offsetOfDeleteLine;
      }
    }
  }
}
