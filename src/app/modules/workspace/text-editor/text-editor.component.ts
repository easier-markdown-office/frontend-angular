import { Component, ElementRef, ViewChild } from '@angular/core';
import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { CursorService } from '../../../core/services/cursor/cursor.service';
import { NotificationCenterService } from '../../../core/services/notification-center/notification-center.service';
import { UserService } from '../../../core/services/user/user.service';
import { ENotificationTrigger } from '../../../core/services/settings/models/notification-trigger.enum';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
})
export class TextEditorComponent {
  @ViewChild('textEditor')
  public textEditor: ElementRef<HTMLTextAreaElement> | undefined;

  @ViewChild('highlighter')
  public highlighter: ElementRef<HTMLTextAreaElement> | undefined;

  private _currentCountOfContributors = 0;
  private _currentDocumentID = '';

  constructor(
    public markdownService: MarkdownService,
    private readonly cursorService: CursorService,
    private readonly notificationCenter: NotificationCenterService,
    private readonly userService: UserService,
  ) {
    this.setNotifier();
  }

  syncScroll(scrollText: HTMLElement, eleHighlighter: HTMLElement): void {
    this.changeHeight();

    eleHighlighter.scrollLeft = scrollText.scrollLeft;
    eleHighlighter.scrollTop = scrollText.scrollTop;
  }

  changeHeight(): void {
    if (!this.textEditor || !this.highlighter) return;

    const firstElement = this.highlighter.nativeElement.firstChild;
    if (!firstElement || !(firstElement instanceof HTMLElement)) return;

    firstElement.style.height =
      this.textEditor.nativeElement.scrollHeight + 'px';
  }

  changeCursor(element: EventTarget | null): void {
    if (!(element instanceof HTMLTextAreaElement)) return;
    this.cursorService.textEditor = this.textEditor?.nativeElement;

    const text = this.markdownService.text.split('');
    let start = element.selectionStart;
    let end = element.selectionEnd;
    for (let i = 0; i < end; ++i) {
      if (text[i] === '\r' || text[i] === '\n') {
        if (i <= start) ++start;

        ++end;
      }
    }

    this.cursorService.changeCursor(
      element.selectionStart,
      element.selectionEnd,
    );
  }

  private setNotifier(): void {
    this._currentDocumentID = this.userService.getCurrentDocument()?.id ?? '';

    this.cursorService.cursors.subscribe((cursors) => {
      const currentDocumentID = this.userService.getCurrentDocument()?.id ?? '';
      if (this._currentDocumentID !== currentDocumentID) {
        this._currentDocumentID = currentDocumentID;
        return;
      }

      const snackerOptions: MatSnackBarConfig = {
        verticalPosition: 'bottom',
        horizontalPosition: 'end',
        duration: 3500,
      };
      const diff = this._currentCountOfContributors - cursors.length;
      if (diff > 0) {
        if (diff === 1)
          this.notificationCenter.notify(
            ENotificationTrigger.contributorJoinedLeft,
            'Contributor left.',
            undefined,
            snackerOptions,
          );
        else
          this.notificationCenter.notify(
            ENotificationTrigger.contributorJoinedLeft,
            'Contributors left.',
            undefined,
            snackerOptions,
          );
      } else if (diff < 0) {
        if (diff === -1)
          this.notificationCenter.notify(
            ENotificationTrigger.contributorJoinedLeft,
            'Contributor joined.',
            undefined,
            snackerOptions,
          );
        else
          this.notificationCenter.notify(
            ENotificationTrigger.contributorJoinedLeft,
            'Contributors joined.',
            undefined,
            snackerOptions,
          );
      }

      this._currentCountOfContributors = cursors.length;
    });
  }
}
