import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { TextEditorComponent } from './text-editor.component';
import { SelectionKeeperDirective } from './selection-keeper.directive';
import { MarkerComponent } from './marker/marker.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { CursorService } from '../../../core/services/cursor/cursor.service';
import { NotificationCenterService } from '../../../core/services/notification-center/notification-center.service';

@NgModule({
  declarations: [
    TextEditorComponent,
    SelectionKeeperDirective,
    MarkerComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AngularResizedEventModule,
    MatCardModule,
    FormsModule,
  ],
  providers: [MarkdownService, CursorService, NotificationCenterService],
  exports: [TextEditorComponent],
  bootstrap: [TextEditorComponent],
})
export class TextEditorModule {}
