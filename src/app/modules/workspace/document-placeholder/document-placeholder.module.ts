import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentPlaceholderComponent } from './document-placeholder.component';

@NgModule({
  declarations: [DocumentPlaceholderComponent],
  imports: [CommonModule],
  bootstrap: [DocumentPlaceholderComponent],
  exports: [DocumentPlaceholderComponent],
})
export class DocumentPlaceholderModule {}
