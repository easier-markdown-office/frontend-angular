import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentPlaceholderComponent } from './document-placeholder.component';

describe('DocumentPlaceholderComponent', () => {
  let component: DocumentPlaceholderComponent;
  let fixture: ComponentFixture<DocumentPlaceholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentPlaceholderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
