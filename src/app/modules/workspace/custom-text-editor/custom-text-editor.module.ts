import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomTextEditorComponent } from './custom-text-editor.component';

@NgModule({
  declarations: [CustomTextEditorComponent],
  imports: [CommonModule],
  exports: [CustomTextEditorComponent],
  bootstrap: [CustomTextEditorComponent],
})
export class CustomTextEditorModule {}
