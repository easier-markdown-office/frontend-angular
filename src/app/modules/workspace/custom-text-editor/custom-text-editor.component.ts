import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-custom-text-editor',
  templateUrl: './custom-text-editor.component.html',
  styleUrls: ['./custom-text-editor.component.scss'],
})
export class CustomTextEditorComponent {
  @Input('lines')
  public lines: string[] = [];
}
