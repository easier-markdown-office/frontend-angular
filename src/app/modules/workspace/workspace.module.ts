import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarkdownPreviewModule } from './markdown-preview/markdown-preview.module';
import { SplitterModule } from './splitter/splitter.module';
import { MenuStripModule } from './menu-strip/menu-strip.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { WorkspaceComponent } from './workspace.component';
import { FooterModule } from '../../core/footer/footer.module';
import { ContributorsDialogModule } from './contributors-dialog/contributors-dialog.module';

@NgModule({
  declarations: [WorkspaceComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ContributorsDialogModule,
    FooterModule,
    MarkdownPreviewModule,
    SplitterModule,
    MenuStripModule,
  ],
  exports: [WorkspaceComponent],
})
export class WorkspaceModule {}
