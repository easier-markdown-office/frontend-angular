import { Component, ViewChild } from '@angular/core';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { CursorService } from '../../../core/services/cursor/cursor.service';
import DocumentDTO from '../../../../submodule-models/document.dto';
import { UserService } from '../../../core/services/user/user.service';
import { ContextMenuComponent } from '../context-menu/context-menu.component';
import { WSService } from '../../../core/services/ws/ws.service';
import { DOCUMENT_MESSAGES } from '../../../../submodule-models/gateway-addresses.constants';
import { NotificationCenterService } from '../../../core/services/notification-center/notification-center.service';
import { SettingsService } from '../../../core/services/settings/settings.service';

const DEBUG_TIMEOUT_MS = 10; // when greater than ~100ms -> problems!

@Component({
  selector: 'app-room-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss'],
})
export class DocumentListComponent {
  private static LOREM_IPSUM =
    'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,';
  private static LOREM_IPSUM_WORD_AMOUNT =
    DocumentListComponent.LOREM_IPSUM.split(/\s/).length + 1;
  private static LOREM_IPSUM_AVG_WORD_LENGTH = Math.ceil(
    DocumentListComponent.LOREM_IPSUM.length /
      DocumentListComponent.LOREM_IPSUM_WORD_AMOUNT,
  );
  private static ONE_MIN_IN_MS = 1000 * 60 * 1;

  @ViewChild(ContextMenuComponent, { static: true })
  public contextMenu?: ContextMenuComponent;

  public documents: DocumentDTO[] = [];
  public collaboratorDocuments: DocumentDTO[] = [];
  public currentDocument: DocumentDTO | undefined;

  // private _loremIpsumCurrentOffset = 0;
  // private _loremCharsToTyping = -1;
  public loremIpsumTimer = -1;

  constructor(
    private readonly cursorService: CursorService,
    private readonly notificationCenter: NotificationCenterService,
    public readonly markdownService: MarkdownService,
    public readonly wsService: WSService,
    public readonly userService: UserService,
    public readonly settingsService: SettingsService,
  ) {
    this.setNotifier();

    this.userService.documents.subscribe((docs) => {
      this.documents = [...docs];
    });
    this.userService.collaboratorDocuments.subscribe((docs) => {
      this.collaboratorDocuments = [...docs];
    });
    this.userService.currentDocument.subscribe((doc) => {
      this.currentDocument = doc;
    });
  }

  sendA(count: number): void {
    this.send(''.padStart(count, 'EDCBA'));
  }

  sendB(count: number): void {
    this.send(''.padStart(count, 'FGHIJ'), true);
  }

  loremIpsum(wpm: number): void {
    const loremCharsToTyping =
      DocumentListComponent.LOREM_IPSUM_AVG_WORD_LENGTH * wpm;
    const sendInterval = Math.ceil(
      DocumentListComponent.ONE_MIN_IN_MS / loremCharsToTyping,
    );
    let loremIpsumCurrentOffset = 0;

    const timerFunc = () => {
      if (loremIpsumCurrentOffset >= loremCharsToTyping) {
        clearInterval(this.loremIpsumTimer);
        this.loremIpsumTimer = -1;
        return;
      }

      const cursorPos = this.cursorService.textEditor?.selectionStart ?? 0;
      const text = this.markdownService.text;
      const before = text.substring(0, cursorPos);
      const after = text.substring(cursorPos);
      this.markdownService.lastDifferent.splice(0);
      this.markdownService.text = `${before}${DocumentListComponent.LOREM_IPSUM.substring(
        loremIpsumCurrentOffset,
        loremIpsumCurrentOffset + 1,
      )}${after}`;
      this.cursorService.changeCursorDebug(
        (this.cursorService.textEditor?.selectionStart ?? 0) + 1,
      );

      ++loremIpsumCurrentOffset;
    };
    this.loremIpsumTimer = setInterval(timerFunc.bind(this), sendInterval);
  }

  loremIpsumCancel(): void {
    clearInterval(this.loremIpsumTimer);
    this.loremIpsumTimer = -1;
  }

  callDiff(): void {
    console.log(this.markdownService.getDifferentInLineModifications());
  }

  invalidDTO(): void {
    /*
    this.wsService.socket?.emit(
      DOCUMENT_MESSAGES.changeText,
      {
        basedOn: 10,
        changes: [{ index: -5 }],
      } as any,
      (data: any) => console.log(data),
    );
         */

    this.wsService.socket?.emit(
      DOCUMENT_MESSAGES.discharge,
      { email: 'defaultueberbit.de', id: '5ffc03c902a27930535b6845' },
      (data: any) => console.log(data),
    );
  }

  openContextMenu(event: MouseEvent, item: DocumentDTO): void {
    if (!this.contextMenu) return;

    event.preventDefault();
    event.stopPropagation();

    this.contextMenu.openContextMenu(event, item);
  }

  changeDocument(doc: DocumentDTO, isSelected = false): void {
    if (isSelected) return;

    this.userService.changeDocument(doc);
  }

  private send(string: string, addToEnd = false) {
    const charArray = string.split('');

    this.markdownService.text = !addToEnd
      ? `${charArray[0]}${this.markdownService.text}`
      : `${this.markdownService.text}${charArray[0]}`;
    if (charArray.length > 1)
      setTimeout(
        () => this.send(charArray.splice(1).join(''), addToEnd),
        DEBUG_TIMEOUT_MS,
      );
  }

  private setNotifier(): void {
    /*
    this.userService.collaboratorDocuments.subscribe((documents) => {
      const snackerOptions: MatSnackBarConfig = {
        verticalPosition: 'bottom',
        horizontalPosition: 'start',
        duration: 3500,
      };
      const diff = this.collaboratorDocuments.length - documents.length;
      if (diff > 0) {
        const removedDocuments = this.collaboratorDocuments.filter(
          (d) => !documents.find((d1) => d1.id === d.id),
        );

        if (diff === 1)
          this.notificationCenter.notify(
            ENotificationTrigger.invitedDischargedFromDocument,
            `You have been discharged from document ${removedDocuments[0]?.name}.`,
            undefined,
            snackerOptions,
          );
        else
          this.notificationCenter.notify(
            ENotificationTrigger.invitedDischargedFromDocument,
            'Documents left.',
            undefined,
            snackerOptions,
          );
      } else if (diff < 0) {
        if (diff === -1)
          this.notificationCenter.notify(
            ENotificationTrigger.invitedDischargedFromDocument,
            'Document joined.',
            undefined,
            snackerOptions,
          );
        else
          this.notificationCenter.notify(
            ENotificationTrigger.invitedDischargedFromDocument,
            'Documents joined.',
            undefined,
            snackerOptions,
          );
      }
    });
     */
  }
}
