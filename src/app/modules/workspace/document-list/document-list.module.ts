import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { DocumentListComponent } from './document-list.component';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { CursorService } from '../../../core/services/cursor/cursor.service';
import { RenameComponent } from '../context-menu/rename/rename.component';
import { DeleteComponent } from '../context-menu/delete/delete.component';
import { ContextMenuModule } from '../context-menu/context-menu.module';
import { DialogWithLoadingSpinnerModule } from '../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.module';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [DocumentListComponent, RenameComponent, DeleteComponent],
  imports: [
    BrowserModule,
    CommonModule,
    MatButtonModule,
    MatCardModule,
    ContextMenuModule,
    DialogWithLoadingSpinnerModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
  ],
  providers: [MarkdownService, CursorService],
  exports: [DocumentListComponent],
  bootstrap: [DocumentListComponent],
})
export class DocumentListModule {}
