import { Component } from '@angular/core';
import { SettingsService } from '../../../core/services/settings/settings.service';
import { UserService } from '../../../core/services/user/user.service';
/*
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
 */

@Component({
  selector: 'app-splitter',
  templateUrl: './splitter.component.html',
  styleUrls: ['./splitter.component.scss'],
  /*
  animations: [
    trigger('fadeAnimation', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate('250ms', style({ width: 'auto', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ width: 'auto', opacity: 1 }),
        animate('250ms', style({ width: 0, opacity: 0 })),
      ]),
    ]),
  ],
   */
})
export class SplitterComponent {
  public title = 'frontend';

  public isDocumentSelected = false;

  constructor(
    public readonly settingsService: SettingsService,
    public readonly userService: UserService,
  ) {
    userService.currentDocument.subscribe(
      (doc) => (this.isDocumentSelected = !!doc),
    );
  }
}
