import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSplitModule } from 'ngx-split';
import { DocumentListModule } from '../document-list/document-list.module';

import { TextEditorModule } from '../text-editor/text-editor.module';
import { MarkdownPreviewModule } from '../markdown-preview/markdown-preview.module';
import { SplitterComponent } from './splitter.component';
import { WSService } from '../../../core/services/ws/ws.service';
import { SettingsService } from '../../../core/services/settings/settings.service';
import { DocumentPlaceholderModule } from '../document-placeholder/document-placeholder.module';

@NgModule({
  declarations: [SplitterComponent],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    DocumentListModule,
    NgxSplitModule.forRoot(),
    TextEditorModule,
    MarkdownPreviewModule,
    DocumentPlaceholderModule,
  ],
  providers: [WSService, SettingsService],
  bootstrap: [SplitterComponent],
  exports: [SplitterComponent],
})
export class SplitterModule {}
