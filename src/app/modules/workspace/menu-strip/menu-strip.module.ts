import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MenuStripComponent } from './menu-strip.component';
import { SettingsService } from '../../../core/services/settings/settings.service';
import { DocumentCreatorComponent } from './document-creator/document-creator.component';
import { FormsModule } from '@angular/forms';
import { SettingsComponent } from './settings/settings.component';
import { WSService } from '../../../core/services/ws/ws.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { DialogWithLoadingSpinnerModule } from '../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.module';
import { TabBehaviourComponent } from './settings/tab-behaviour/tab-behaviour.component';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    MenuStripComponent,
    DocumentCreatorComponent,
    SettingsComponent,
    TabBehaviourComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    FormsModule,
    DialogWithLoadingSpinnerModule,
    MatSelectModule,
  ],
  providers: [SettingsService, WSService],
  exports: [MenuStripComponent],
  bootstrap: [MenuStripComponent],
})
export class MenuStripModule {}
