import { Component, ViewChild } from '@angular/core';
// import { MatDialogRef } from '@angular/material/dialog';
import { WSService } from '../../../../core/services/ws/ws.service';
import { DOCUMENT_MESSAGES } from '../../../../../submodule-models/gateway-addresses.constants';
import { DialogWithLoadingSpinnerComponent } from '../../../../core/dialog-with-loading-spinner/dialog-with-loading-spinner.component';
import { isDocumentNameValid } from '../../../../../submodule-models/utitlity.static';

@Component({
  selector: 'app-document-creator',
  templateUrl: './document-creator.component.html',
  styleUrls: ['./document-creator.component.scss'],
})
export class DocumentCreatorComponent {
  @ViewChild('dialog', { static: true })
  public dialogComponent: DialogWithLoadingSpinnerComponent | undefined;

  public fileName = '';

  constructor(
    private readonly wsService: WSService, // private readonly dialogRef: MatDialogRef<DocumentCreatorComponent>,
  ) {
    this.nameIsValidate = isDocumentNameValid;
  }

  public nameIsValidate: (name: string) => boolean = () => true;

  async emitButtonSubmit(): Promise<void> {
    if (
      !this.dialogComponent ||
      !this.dialogComponent.submitButton ||
      this.dialogComponent.submitButton.disabled
    )
      return;

    await this.dialogComponent.submit();
  }

  async submit(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.wsService.socket?.emit(
        DOCUMENT_MESSAGES.create,
        this.fileName,
        (data: any) => {
          console.log(data);
          resolve(true);
        },
      );
    });
  }
}
