import { Component } from '@angular/core';
import { downloadText } from '../../../shared/utility';
import { MarkdownService } from '../../../core/services/markdown/markdown.service';
import { WSService } from '../../../core/services/ws/ws.service';
import { SettingsService } from '../../../core/services/settings/settings.service';
import { UserService } from '../../../core/services/user/user.service';
import { DocumentCreatorComponent } from './document-creator/document-creator.component';
import { MatDialog } from '@angular/material/dialog';
import { SettingsComponent } from './settings/settings.component';
import { FetcherService } from '../../../core/services/fetcher/fetcher.service';
import { ContributorsDialogComponent } from '../contributors-dialog/contributors-dialog.component';

@Component({
  selector: 'app-menu-strip',
  templateUrl: './menu-strip.component.html',
  styleUrls: ['./menu-strip.component.scss'],
})
export class MenuStripComponent {
  public username?: string;

  private _fileName = '';

  constructor(
    private readonly markdownService: MarkdownService,
    private readonly fetchService: FetcherService,
    private readonly dialog: MatDialog,
    public readonly socketService: WSService,
    public readonly settingsService: SettingsService,
    public readonly userService: UserService,
  ) {
    this.username = userService.getUsername();

    this.userService.currentDocument.subscribe((doc) => {
      this._fileName = doc?.name ?? '';
    });

    this.userService.username.subscribe(
      (username) => (this.username = username),
    );
  }

  downloadFile(): void {
    downloadText(this.markdownService.text, `${this._fileName}.md`);
  }

  openDocumentCreateDialog(): void {
    // TODO: dynamic size
    this.dialog.open(DocumentCreatorComponent, {
      width: '550px',
      disableClose: true,
    });
  }

  openContributorsDialog(): void {
    const dialogRef = this.dialog.open(ContributorsDialogComponent, {
      width: '550px',
      data: this.userService.getCurrentDocument(),
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  openSettingsDialog(): void {
    // TODO: dynamic size
    const dialogRef = this.dialog.open(SettingsComponent, {
      width: '1000px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  logout(): void {
    this.fetchService.logout();
  }
}
