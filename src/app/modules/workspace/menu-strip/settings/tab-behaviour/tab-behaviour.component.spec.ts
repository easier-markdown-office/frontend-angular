import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabBehaviourComponent } from './tab-behaviour.component';

describe('TabBehaviourComponent', () => {
  let component: TabBehaviourComponent;
  let fixture: ComponentFixture<TabBehaviourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TabBehaviourComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabBehaviourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
