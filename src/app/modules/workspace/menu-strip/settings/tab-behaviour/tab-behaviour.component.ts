import { Component } from '@angular/core';
import { PushNotificationService } from '../../../../../core/services/push-notifiction/push-notification.service';
import { ENotificationType } from '../../../../../core/services/settings/models/notification-type.enum';
import { SettingsService } from '../../../../../core/services/settings/settings.service';

@Component({
  selector: 'app-tab-behaviour',
  templateUrl: './tab-behaviour.component.html',
  styleUrls: ['./tab-behaviour.component.scss'],
})
export class TabBehaviourComponent {
  public notificationTypes: {
    display: string;
    value: ENotificationType;
    disabled?: boolean;
  }[] = [
    {
      display: 'None',
      value: ENotificationType.none,
    },
    {
      display: 'Push Notifications & In-App-Notifications',
      value: ENotificationType.inApp & ENotificationType.os,
    },
    {
      display: 'Push Notifications (only when inactive)',
      value: ENotificationType.os,
    },
    {
      display: 'In-App-Notifications (only when active)',
      value: ENotificationType.inApp,
    },
  ];
  public selectedNotType: ENotificationType;

  constructor(
    private readonly _pushService: PushNotificationService,
    private readonly _settingsService: SettingsService,
  ) {
    this.selectedNotType = _settingsService.notificationType;
    const pushNot = this.notificationTypes.find(
      (n) => n.value === ENotificationType.os,
    );
    if (pushNot) pushNot.disabled = !this._pushService.isSupported;
  }

  requestPushNotifications(): void {
    console.log('test');
    this._pushService.requestPermission();
  }
}
