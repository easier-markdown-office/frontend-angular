import { Component, OnInit } from '@angular/core';
import { FetcherService } from '../../core/services/fetcher/fetcher.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-start-screen',
  templateUrl: './start-screen.component.html',
  styleUrls: ['./start-screen.component.scss'],
})
export class StartScreenComponent implements OnInit {
  public isLoadingAutoSignIn = false;

  constructor(
    private readonly socketService: FetcherService,
    private readonly _snackBar: MatSnackBar,
  ) {}

  async ngOnInit(): Promise<void> {
    if (!this.socketService.canTryLoginWithToken()) return;

    this.isLoadingAutoSignIn = true;

    if (!(await this.socketService.loginWithLocalToken())) {
      this._snackBar.open(
        'Authentication failed! Looks like your token has expired.',
        'Okay!',
        {
          duration: 8000,
        },
      );
      this.isLoadingAutoSignIn = false;
    }
  }
}
