import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginScreenComponent } from './login-screen/login-screen.component';
import { StartScreenComponent } from './start-screen.component';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AutoLoginComponent } from './auto-login/auto-login.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    LoginScreenComponent,
    StartScreenComponent,
    WelcomeScreenComponent,
    AutoLoginComponent,
  ],
  imports: [
    CommonModule,
    MatDividerModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
  ],
  exports: [StartScreenComponent],
})
export class StartScreenModule {}
