import { Component } from '@angular/core';
import LoginDataDTO from '../../../../submodule-models/login-data.dto';
import { FetcherService } from '../../../core/services/fetcher/fetcher.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PASSWORD_MIN_LENGTH } from '../../../../submodule-models/constants';

@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.scss'],
})
export class LoginScreenComponent {
  public loginData: LoginDataDTO = { user: 'default', password: 'password' }; // TODO: remove it, just debug

  public isLoading = false;

  public readonly passwordMinLen: number = 1;

  constructor(
    private readonly fetchService: FetcherService,
    private readonly _snackBar: MatSnackBar,
  ) {
    this.passwordMinLen = PASSWORD_MIN_LENGTH;
  }

  async submit(): Promise<void> {
    this.isLoading = true;
    const errorMessage = await this.fetchService.login(this.loginData);
    if (errorMessage) {
      this._snackBar.open(errorMessage, 'Okay!', {
        duration: 8000,
      });
    }
    this.isLoading = false;
  }
}
